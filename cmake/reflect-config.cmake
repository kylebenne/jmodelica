
file(GLOB class_files RELATIVE "${jmodelica_bin_path}" "${jmodelica_bin_path}/org/jmodelica/modelica/compiler/*.class")

foreach(file IN LISTS class_files)
  string(REPLACE ".class" "" file ${file})
  string(REPLACE "/" "." file ${file})
  list(APPEND class_list ${file})
endforeach()

set(reflect_config_string "\
[\n\
{\n\
  \"name\":\"beaver.Symbol\",\n\
  \"fields\":[{\"name\":\"value\", \"allowWrite\": true}],\n\
  \"allDeclaredConstructors\" : true,\n\
  \"allPublicConstructors\" : true,\n\
  \"allDeclaredMethods\" : true,\n\
  \"allPublicMethods\" : true,\n\
  \"allDeclaredClasses\" : true,\n\
  \"allPublicClasses\" : true\n\
},\n\
{\n\
  \"name\":\"org.jmodelica.modelica.compiler.StepInfo$MemoryChangeItem\",\n\
  \"methods\":[{\"name\":\"<init>\",\"parameterTypes\":[] }]\n\
},\n\
{\n\
  \"name\":\"org.jmodelica.modelica.compiler.StepInfo$TimeItem\",\n\
  \"methods\":[{\"name\":\"<init>\",\"parameterTypes\":[] }]\n\
}\n\
")

file(WRITE ${reflect_config_path} ${reflect_config_string})

foreach(class IN LISTS class_list)
  string(FIND ${class} "Generator" position)
  if(position GREATER 0)
    set(class_conf_string ",\n\
{\n\
  \"name\":\"${class}\",\n\
  \"allDeclaredConstructors\" : true,\n\
  \"allPublicConstructors\" : true\n\
}\
  ")
    file(APPEND ${reflect_config_path} ${class_conf_string})
  endif()
endforeach()

file(APPEND ${reflect_config_path} "\n]\n")


